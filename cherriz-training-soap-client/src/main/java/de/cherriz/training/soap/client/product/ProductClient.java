package de.cherriz.training.soap.client.product;

import de.cherriz.training.soap.client.product.cxf.GetIdRequest;
import de.cherriz.training.soap.client.product.cxf.GetProductRequest;
import de.cherriz.training.soap.client.product.cxf.ProductsPort;
import de.cherriz.training.soap.client.product.cxf.ProductsPortService;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Pattern;

public class ProductClient {

    public static void main(String[] args) throws MalformedURLException {
        // Für Lokal: http://localhost:9002/ws/products.wsdl
        ProductsPortService service = new ProductsPortService(new URL("http://mib-hp-sose19.iap.hs-heilbronn.de:9002/ws/products.wsdl"));
        ProductsPort port = service.getProductsPortSoap11();

        for (String arg : args) {
            if (Pattern.matches("'[0-9]+'", "'"+arg+"'" )) {
                GetProductRequest request = new GetProductRequest();
                request.setId(Long.valueOf(arg));
                System.out.println("getProduct(" + arg + "): " + port.getProduct(request).getProduct().getName());
            } else {
                GetIdRequest request = new GetIdRequest();
                request.setName(arg);
                System.out.println("getId(" + arg + "): " + port.getId(request).getId());
            }
        }
    }

}
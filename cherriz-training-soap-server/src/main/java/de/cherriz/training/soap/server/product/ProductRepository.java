package de.cherriz.training.soap.server.product;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.LinkedList;
import java.util.List;

@Component
public class ProductRepository {

    private static final List<Product> products = new LinkedList<>();

    @PostConstruct
    public void initData() {
        Product banana = new Product();
        banana.setId(1L);
        banana.setName("Banane");
        banana.setPrice(3.90f);

        products.add(banana);

        Product apple = new Product();
        apple.setId(2L);
        apple.setName("Apfel");
        apple.setPrice(2.90f);

        products.add(apple);

        Product pear = new Product();
        pear.setId(3L);
        pear.setName("Birne");
        pear.setPrice(2.49f);

        products.add(pear);
    }

    public Product findProductById(Long id) {
        Assert.notNull(id, "The products's id must not be null");
        return products.stream().filter(p->p.getId() == id).findFirst().orElse(null);
    }

    public Product findProductByName(String name) {
        Assert.notNull(name, "The products's name must not be null");
        return products.stream().filter(p->p.getName().equals(name)).findFirst().orElse(null);
    }

}

//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2018.03.03 um 02:03:15 PM CET 
//

@javax.xml.bind.annotation.XmlSchema(namespace = "http://cherriz.de/training/soap/server/product", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package de.cherriz.training.soap.server.product;

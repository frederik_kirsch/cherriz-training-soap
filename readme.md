Entwickelt für Java 11.

URL für Zugriff auf lokalen SOAP Service:
`http://localhost:9002/ws/products.wsdl`

URL für Zugriff auf im HHN Netz gehosteten SOAP Service:
`http://mib-hp-sose19.iap.hs-heilbronn.de:9002/ws/products.wsdl`